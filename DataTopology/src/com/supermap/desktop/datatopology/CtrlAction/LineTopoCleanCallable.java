package com.supermap.desktop.datatopology.CtrlAction;

import java.util.concurrent.CancellationException;

import com.supermap.data.DatasetVector;
import com.supermap.data.Datasource;
import com.supermap.data.SteppedEvent;
import com.supermap.data.SteppedListener;
import com.supermap.data.topology.TopologyProcessing;
import com.supermap.data.topology.TopologyProcessingOptions;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.datatopology.DataTopologyProperties;
import com.supermap.desktop.progress.Interface.UpdateProgressCallable;

public class LineTopoCleanCallable extends UpdateProgressCallable {
	private String datasetName;
	private TopologyProcessingOptions topologyProcessingOptions;
	private Datasource datasource;

	public LineTopoCleanCallable(String datasetName, TopologyProcessingOptions topologyProcessingOptions, Datasource datasource) {
		this.datasetName = datasetName;
		this.datasource = datasource;
		this.topologyProcessingOptions = topologyProcessingOptions;
	}

	@Override
	public Boolean call() throws Exception {
		boolean result = true;

		try {
			DatasetVector dataset = (DatasetVector) CommonToolkit.DatasetWrap.getDatasetFromDatasource(datasetName, datasource);
			String topoInfo = "";
			TopologyProcessing.addSteppedListener(new PercentListener());
			boolean topologyProcessResult = TopologyProcessing.clean(dataset, topologyProcessingOptions);
			if (topologyProcessResult) {
				topoInfo = DataTopologyProperties.getString("String_TopoLineSucceed").replace("{0}", datasetName);
			} else {
				topoInfo = DataTopologyProperties.getString("String_TopoLineFailed").replace("{0}", datasetName);
			}
			Application.getActiveApplication().getOutput().output(topoInfo);
		} catch (Exception e) {
			result = false;
			Application.getActiveApplication().getOutput().output(e);
		}
		return result;
	}

	class PercentListener implements SteppedListener {

		@Override
		public void stepped(SteppedEvent arg0) {
			try {
				updateProgress(arg0.getPercent(), String.valueOf(arg0.getRemainTime()), arg0.getMessage());
			} catch (CancellationException e) {
				arg0.setCancel(true);
			}

		}

	}

}
