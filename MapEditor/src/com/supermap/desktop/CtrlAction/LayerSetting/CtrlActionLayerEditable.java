package com.supermap.desktop.CtrlAction.LayerSetting;

import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.mapping.Layer;
import com.supermap.mapping.LayerGroup;
import com.supermap.mapping.ThemeCustom;
import com.supermap.mapping.ThemeRange;
import com.supermap.mapping.ThemeType;
import com.supermap.mapping.ThemeUnique;

public class CtrlActionLayerEditable extends CtrlAction {

	public CtrlActionLayerEditable(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		try {
			IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
			boolean isEditable = !formMap.getActiveLayers()[0].isEditable();
			if (formMap.getMapControl().isMultiLayerEditEnabled()) {
				for (Layer layer : formMap.getActiveLayers()) {
					layer.setEditable(isEditable);
				}
			} else {
				formMap.getActiveLayers()[0].setEditable(isEditable);
			}

			if (isEditable) {
				formMap.getMapControl().setActiveEditableLayer(formMap.getActiveLayers()[0]);
			}
			formMap.getMapControl().getMap().refresh();
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		boolean enable = false;
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		if (formMap != null) {
			for (Layer layer : formMap.getActiveLayers()) {
				DatasetVector datasetVector = (DatasetVector) layer.getDataset();
				if (datasetVector != null) {// 网络节点数据集不可编辑
					if (!layer.getDataset().isReadOnly() && layer.isVisible() && layer.getTheme() != null) {
						if (layer.getTheme() instanceof ThemeUnique || layer.getTheme() instanceof ThemeRange || layer.getTheme() instanceof ThemeCustom) {
							enable = true;
							break;
						}
					} else if (!layer.getDataset().isReadOnly() && layer.isVisible() && layer.getDataset() instanceof DatasetVector) {
						// 三维网络数据集支持在地图中编辑
						enable = true;
						break;
					}
				}
			}

			// 实现多选的可用控制
			for (Layer layer : formMap.getActiveLayers()) {
				if (layer.getDataset() == null) {
					enable = false;
					break;
				}
				if (layer.getDataset() != null && (layer.getDataset().getType() == DatasetType.GRID || layer.getDataset().getType() == DatasetType.IMAGE)
						|| layer instanceof LayerGroup) {
					enable = false;
					break;
				}
				if (layer.getDataset() != null && layer.getTheme() != null && layer.getTheme().getType() != ThemeType.UNIQUE
						&& layer.getTheme().getType() != ThemeType.RANGE && layer.getTheme().getType() != ThemeType.CUSTOM) {
					enable = false;
					break;
				}
			}
		}
		return enable;
	}

	@Override
	public boolean check() {
		boolean check = false;
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		if (formMap.getActiveLayers()[0].isEditable()) {
			check = true;
		}

		return check;
	}
}