package com.supermap.desktop.CtrlAction.Dataset;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JToolBar;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;

import com.supermap.data.Charset;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.Datasets;
import com.supermap.data.Datasource;
import com.supermap.data.EncodeType;
import com.supermap.data.Workspace;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.dataeditor.DataEditorProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.controls.CommonTableRender;
import com.supermap.desktop.ui.controls.DataCell;
import com.supermap.desktop.ui.controls.DatasetComboBox;
import com.supermap.desktop.ui.controls.DatasourceComboBox;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.SmDialog;
import com.supermap.desktop.ui.controls.mutiTable.DDLExportTableModel;
import com.supermap.desktop.ui.controls.mutiTable.component.ComboBoxCellEditor;
import com.supermap.desktop.ui.controls.mutiTable.component.MutiTable;

import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class JDialogDatasetNew extends SmDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int COLUMN_INDEX_INDEX = 0;
	private static final int COLUMN_INDEX_TargetDatasource = 1;
	private static final int COLUMN_INDEX_DatasetType = 2;
	private static final int COLUMN_INDEX_DatasetName = 3;
	private static final int COLUMN_INDEX_EncodeType = 4;
	private static final int COLUMN_INDEX_Charset = 5;
	private static final int COLUMN_INDEX_WindowMode = 6;

	final JPanel contentPanel = new JPanel();
	private JToolBar toolBar;
	private JButton buttonSelectAll;
	private JButton buttonSelectInvert;
	private JButton buttonDelete;
	private JButton buttonSetting;
	private MutiTable table;
	private JCheckBox chckbxAutoClose;
	private JButton okButton;
	private JButton cancelButton;
	private boolean isDatasetTypeValueChanged;
	private String defaultDatasetName = "";

	/**
	 * Create the dialog.
	 */
	public JDialogDatasetNew() {
		this.setModal(true);
		setTitle("Template");
		setBounds(100, 100, 677, 405);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		toolBar = new JToolBar();
		toolBar.setFloatable(false);

		table = new MutiTable();
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		DDLExportTableModel tableModel = new DDLExportTableModel(new String[] { "Index", "TargetDatasource", "CreateType", "DatasetName", "EncodeType",
				"Charset", "AddToMap" }) {
			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, true, true, true, true, true, true };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};

		table.setModel(tableModel);
		table.putClientProperty("terminateEditOnFocusLost", true);
		initializeColumns();

		table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				table_ValueChanged(e);
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (1 == e.getClickCount()) {
					int last = table.getRowCount() - 1;
					int select = table.getSelectedRow();
					if (last == select) {
						buttonDelete.setEnabled(false);
					} else {
						buttonDelete.setEnabled(true);
					}
				}
			}
		});
		table.setRowHeight(23);
		JScrollPane scrollPaneTable = new JScrollPane(table);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);

		// @formatter:off
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
				.addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(7)
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
		);
		// @formatter:on

		buttonSelectAll = new JButton();
		buttonSelectAll
				.setIcon(new ImageIcon(JDialogDatasetNew.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_SelectAll.png")));
		buttonSelectAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSelectAll_Click();
			}
		});
		toolBar.add(buttonSelectAll);

		buttonSelectInvert = new JButton();
		buttonSelectInvert.setIcon(new ImageIcon(JDialogDatasetNew.class
				.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_SelectInverse.png")));
		buttonSelectInvert.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSelectInvert_Click();
			}
		});
		this.toolBar.add(buttonSelectInvert);
		this.toolBar.add(createSeparator());

		this.buttonDelete = new JButton();
		this.buttonDelete
				.setIcon(new ImageIcon(JDialogDatasetNew.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_Delete.png")));
		this.buttonDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonDelete_Click();
			}
		});
		buttonDelete.setHorizontalAlignment(SwingConstants.LEFT);
		toolBar.add(buttonDelete);
		this.toolBar.add(createSeparator());

		buttonSetting = new JButton();
		buttonSetting.setIcon(new ImageIcon(JDialogDatasetNew.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_Setting.PNG")));
		buttonSetting.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSetting_Click();
			}
		});
		toolBar.add(buttonSetting);
		contentPanel.setLayout(gl_contentPanel);

		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		chckbxAutoClose = new JCheckBox("Auto Close");
		chckbxAutoClose.setVerticalAlignment(SwingConstants.TOP);
		chckbxAutoClose.setHorizontalAlignment(SwingConstants.LEFT);
		chckbxAutoClose.setSelected(true);
		okButton = new JButton();
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButton_Click();
			}
		});
		getRootPane().setDefaultButton(okButton);
		cancelButton = new JButton();
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButton_Click();
			}
		});

		// @formatter:off
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(gl_buttonPane.createParallelGroup(Alignment.TRAILING).addGroup(
				gl_buttonPane.createSequentialGroup().addContainerGap().addComponent(chckbxAutoClose)
						.addPreferredGap(ComponentPlacement.RELATED, 227, Short.MAX_VALUE).addComponent(okButton, 75, 75, 75)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(cancelButton, 75, 75, 75).addContainerGap()));
		gl_buttonPane.setVerticalGroup(gl_buttonPane.createParallelGroup(Alignment.LEADING).addGroup(
				gl_buttonPane
						.createSequentialGroup()
						.addGap(5)
						.addGroup(
								gl_buttonPane.createParallelGroup(Alignment.BASELINE, false).addComponent(okButton).addComponent(chckbxAutoClose)
										.addComponent(cancelButton)).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		buttonPane.setLayout(gl_buttonPane);
		// @formatter:on

		initializeResources();
		this.setLocationRelativeTo(null);

		// 添加默认要创建的数据集行
		copyPreDatasetInfo(-1);

		// 添加预加载的一行
		copyPreDatasetInfo(0);
	}

	public boolean isAutoClose() {
		return this.chckbxAutoClose.isSelected();
	}

	private void initializeResources() {
		try {
			this.setTitle(DataEditorProperties.getString("String_ToolStripMenuItem_NewDataset"));
			this.cancelButton.setText(CommonProperties.getString("String_Button_Cancel"));
			this.okButton.setText(CommonProperties.getString("String_Button_OK"));

			table.getColumnModel().getColumn(COLUMN_INDEX_INDEX).setHeaderValue(CommonProperties.getString("String_ColumnHeader_Index"));
			table.getColumnModel().getColumn(COLUMN_INDEX_TargetDatasource).setHeaderValue(CommonProperties.getString("String_ColumnHeader_TargetDatasource"));
			table.getColumnModel().getColumn(COLUMN_INDEX_DatasetType).setHeaderValue(DataEditorProperties.getString("String_CreateType"));
			table.getColumnModel().getColumn(COLUMN_INDEX_DatasetName).setHeaderValue(DataEditorProperties.getString("String_ColumnTitle_DtName"));
			table.getColumnModel().getColumn(COLUMN_INDEX_EncodeType).setHeaderValue(CommonProperties.getString("String_ColumnHeader_EncodeType"));
			table.getColumnModel().getColumn(COLUMN_INDEX_Charset).setHeaderValue(DataEditorProperties.getString("String_Charset"));
			table.getColumnModel().getColumn(COLUMN_INDEX_WindowMode).setHeaderValue(DataEditorProperties.getString("String_DataGridViewComboBoxColumn_Name"));

			buttonSelectAll.setToolTipText(CommonProperties.getString("String_ToolBar_SelectAll"));
			buttonSelectInvert.setToolTipText(CommonProperties.getString("String_ToolBar_SelectInverse"));
			buttonDelete.setToolTipText(CommonProperties.getString("String_Delete"));
			buttonSetting.setToolTipText(CommonProperties.getString("String_ToolBar_SetBatch"));
			this.chckbxAutoClose.setText(CommonProperties.getString("String_CheckBox_CloseDialog"));
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	public NewDatasetInfo[] getDatasets() {
		ArrayList<NewDatasetInfo> infos = new ArrayList<NewDatasetInfo>();
		try {
			String targetDatasource = this.table.getModel().getValueAt(0, COLUMN_INDEX_TargetDatasource).toString();
			if ("".equals(targetDatasource)) {
				return new NewDatasetInfo[0];
			}
			for (int index = 0; index < this.table.getRowCount() - 1; index++) {
				NewDatasetInfo info = new NewDatasetInfo();
				String datasourceName = this.table.getModel().getValueAt(index, COLUMN_INDEX_TargetDatasource).toString();
				Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(datasourceName);
				info.setTargetDatasource(datasource);

				String typeName = this.table.getModel().getValueAt(index, COLUMN_INDEX_DatasetType).toString();
				DatasetType datasetType = CommonToolkit.DatasetTypeWrap.findType(typeName);
				info.setDatasetType(datasetType);

				String datasetName = this.table.getModel().getValueAt(index, COLUMN_INDEX_DatasetName).toString();
				info.setDatasetName(datasetName);

				String encodeName = this.table.getModel().getValueAt(index, COLUMN_INDEX_EncodeType).toString();
				EncodeType encodeType = CommonToolkit.EncodeTypeWrap.findType(encodeName);
				info.setEncodeType(encodeType);

				String charsetName = this.table.getModel().getValueAt(index, COLUMN_INDEX_Charset).toString();
				Charset charset = CommonToolkit.CharsetWrap.getCharset(charsetName);
				info.setCharset(charset);

				String modeTypeName = this.table.getModel().getValueAt(index, COLUMN_INDEX_WindowMode).toString();
				AddToWindowMode modeType = AddToWindowMode.getWindowMode(modeTypeName);
				info.setModeType(modeType);

				infos.add(info);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return infos.toArray(new NewDatasetInfo[infos.size()]);
	}

	/**
	 * 初始化列表的列枚举类型
	 */
	@SuppressWarnings("unchecked")
	private void initializeColumns() {
		try {
			// 目标数据源
			int count = Application.getActiveApplication().getWorkspace().getDatasources().getCount();
			String[] datasources = new String[count];
			int selectedIndex = -1;
			String activeDatasource = "";
			if (0 < Application.getActiveApplication().getActiveDatasources().length) {
				activeDatasource = Application.getActiveApplication().getActiveDatasources()[0].getAlias();
			}

			for (int index = 0; index < count; index++) {
				datasources[index] = Application.getActiveApplication().getWorkspace().getDatasources().get(index).getAlias();
				if (datasources[index].equals(activeDatasource)) {
					selectedIndex = index;
				}
			}
			final DatasourceComboBox datasourceComboBox = new DatasourceComboBox(Application.getActiveApplication().getWorkspace().getDatasources());
			datasourceComboBox.setSelectedIndex(selectedIndex);
			datasourceComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						// 选择不同的数据源时更新要新建的数据集的名称
						String item = (String) datasourceComboBox.getSelectItem();
						Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(item);
						String datasetName = table.getValueAt(table.getSelectedRow(), COLUMN_INDEX_DatasetName).toString();
						table.getModel().setValueAt(datasource.getDatasets().getAvailableDatasetName(datasetName), table.getSelectedRow(),
								COLUMN_INDEX_DatasetName);
					} catch (Exception ex) {
						Application.getActiveApplication().getOutput().output(ex);
					}
				}
			});

			DefaultCellEditor targetDatasourceCellEditor = new DefaultCellEditor(datasourceComboBox);
			TableColumn targetDatasourceColumn = table.getColumnModel().getColumn(COLUMN_INDEX_TargetDatasource);
			targetDatasourceColumn.setCellEditor(targetDatasourceCellEditor);
			// 设置渲染
			CommonTableRender renderer = new CommonTableRender();
			targetDatasourceColumn.setCellRenderer(renderer);
			// 可创建的数据集类型
			ArrayList<DatasetType> datasetTypes = new ArrayList<DatasetType>();
			datasetTypes.add(DatasetType.POINT);
			datasetTypes.add(DatasetType.LINE);
			datasetTypes.add(DatasetType.REGION);
			datasetTypes.add(DatasetType.TEXT);
			datasetTypes.add(DatasetType.CAD);
			datasetTypes.add(DatasetType.TABULAR);
			datasetTypes.add(DatasetType.POINT3D);
			datasetTypes.add(DatasetType.LINE3D);
			datasetTypes.add(DatasetType.REGION3D);
			DatasetComboBox comboBoxDatasetType = new DatasetComboBox(datasetTypes.toArray(new DatasetType[datasetTypes.size()]));
			DefaultCellEditor datasetTypeCellEditor = new DefaultCellEditor(comboBoxDatasetType);
			TableColumn datasetTypeColumn = table.getColumnModel().getColumn(COLUMN_INDEX_DatasetType);
			datasetTypeColumn.setCellEditor(datasetTypeCellEditor);
			datasetTypeColumn.setPreferredWidth(100);
			datasetTypeColumn.setCellRenderer(renderer);
			// 编码类型
			ArrayList<String> encodeType = new ArrayList<String>();
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.NONE));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.BYTE));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT16));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT24));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.INT32));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.DCT));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.SGL));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.LZW));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.PNG));
			encodeType.add(CommonToolkit.EncodeTypeWrap.findName(EncodeType.COMPOUND));
			JComboBox<String> encodeTypeComboBox = new JComboBox<String>(new DefaultComboBoxModel<String>(encodeType.toArray(new String[encodeType.size()])));
			TableColumn encodeTypeColumn = table.getColumnModel().getColumn(COLUMN_INDEX_EncodeType);
			TableCellEditor cellEditor = new DefaultCellEditor(encodeTypeComboBox);
			encodeTypeColumn.setCellEditor(cellEditor);
			// 字符集
			ArrayList<String> charsetes = new ArrayList<String>();
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.OEM));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.EASTEUROPE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.THAI));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.RUSSIAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.BALTIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ARABIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HEBREW));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.VIETNAMESE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.TURKISH));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.GREEK));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CHINESEBIG5));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.JOHAB));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.HANGEUL));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SHIFTJIS));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.MAC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.SYMBOL));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.DEFAULT));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.ANSI));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF8));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UTF7));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.WINDOWS1252));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.KOREAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.UNICODE));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.CYRILLIC));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5GERMAN));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5SWEDISH));
			charsetes.add(CommonToolkit.CharsetWrap.getCharsetName(Charset.XIA5NORWEGIAN));
			ComboBoxCellEditor charseteCellEditor = new ComboBoxCellEditor();
			charseteCellEditor.getComboBox().setModel(new DefaultComboBoxModel<Object>(charsetes.toArray(new String[charsetes.size()])));
			TableColumn charseteColumn = table.getColumnModel().getColumn(COLUMN_INDEX_Charset);
			charseteColumn.setCellEditor(charseteCellEditor);

			// 填充添加到图层列
			ArrayList<String> addTos = new ArrayList<String>();
			addTos.add(AddToWindowMode.toString(AddToWindowMode.NONEWINDOW));
			addTos.add(AddToWindowMode.toString(AddToWindowMode.NEWWINDOW));
			if (Application.getActiveApplication().getActiveForm() != null && Application.getActiveApplication().getActiveForm() instanceof IFormMap) {
				addTos.add(AddToWindowMode.toString(AddToWindowMode.CURRENTWINDOW));
			}

			ComboBoxCellEditor addToCellEditor = new ComboBoxCellEditor();
			addToCellEditor.getComboBox().setModel(new DefaultComboBoxModel<Object>(addTos.toArray(new String[addTos.size()])));
			TableColumn addToColumn = table.getColumnModel().getColumn(COLUMN_INDEX_WindowMode);
			addToColumn.setCellEditor(addToCellEditor);

		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	/**
	 * 复制一行记录
	 * 
	 * @param srcIndex
	 *            上一行的索引，如果是-1，则会创建默认的一行
	 */
	private void copyPreDatasetInfo(int rowIndex) {
		try {
			Object[] datas = new Object[7];
			if (rowIndex == -1) {
				datas[COLUMN_INDEX_INDEX] = "1";
				Datasource targetDatasource = null;
				if (0 < Application.getActiveApplication().getActiveDatasources().length) {
					targetDatasource = Application.getActiveApplication().getActiveDatasources()[0];
					String filePath = CommonToolkit.DatasourceImageWrap.getImageIconPath(targetDatasource.getEngineType());
					DataCell datasourceCell = new DataCell(filePath, targetDatasource.getAlias());
					datas[COLUMN_INDEX_TargetDatasource] = datasourceCell;
					String datasetName = this.getDefaultDatasetName(DatasetType.POINT);
					defaultDatasetName = targetDatasource.getDatasets().getAvailableDatasetName(datasetName);
					datas[COLUMN_INDEX_DatasetName] = defaultDatasetName;
				} else {
					datas[COLUMN_INDEX_TargetDatasource] = "";
					datas[COLUMN_INDEX_DatasetName] = "";
				}
				String path = CommonToolkit.DatasetImageWrap.getImageIconPath(DatasetType.POINT);
				String dataTypeName = CommonToolkit.DatasetTypeWrap.findName(DatasetType.POINT);
				DataCell datasetTypeCell = new DataCell(path, dataTypeName);
				datas[COLUMN_INDEX_DatasetType] = datasetTypeCell;

				datas[COLUMN_INDEX_EncodeType] = CommonProperties.getString("String_EncodeType_None");
				datas[COLUMN_INDEX_Charset] = Charset.UTF8;
				datas[COLUMN_INDEX_WindowMode] = AddToWindowMode.toString(AddToWindowMode.NONEWINDOW);
			} else {
				datas[COLUMN_INDEX_INDEX] = this.table.getRowCount() + 1;
				datas[COLUMN_INDEX_TargetDatasource] = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource);
				datas[COLUMN_INDEX_DatasetType] = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_DatasetType);
				datas[COLUMN_INDEX_DatasetName] = "";
				datas[COLUMN_INDEX_EncodeType] = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_EncodeType);
				datas[COLUMN_INDEX_Charset] = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_Charset);
				datas[COLUMN_INDEX_WindowMode] = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_WindowMode);
			}
			this.table.addRow(datas);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private String getDefaultDatasetName(DatasetType datasetType) {
		String newDatasetName = "";
		if (datasetType == DatasetType.POINT) {
			newDatasetName = "New_Point";
		} else if (datasetType == DatasetType.LINE) {
			newDatasetName = "New_Line";
		} else if (datasetType == DatasetType.REGION) {
			newDatasetName = "New_Region";
		} else if (datasetType == DatasetType.TEXT) {
			newDatasetName = "New_Text";
		} else if (datasetType == DatasetType.CAD) {
			newDatasetName = "New_CAD";
		} else if (datasetType == DatasetType.TABULAR) {
			newDatasetName = "New_Tabular";
		} else if (datasetType == DatasetType.POINT3D) {
			newDatasetName = "New_Point3D";
		} else if (datasetType == DatasetType.LINE3D) {
			newDatasetName = "New_Line3D";
		} else if (datasetType == DatasetType.REGION3D) {
			newDatasetName = "New_Region3D";
		} else if (datasetType == DatasetType.PARAMETRICLINE) {
			newDatasetName = "New_ParametricLine";
		} else if (datasetType == DatasetType.PARAMETRICREGION) {
			newDatasetName = "New_ParametricRegion";
		} else if (datasetType == DatasetType.IMAGECOLLECTION) {
			newDatasetName = "New_ImageCollection";
		}

		return newDatasetName;
	}

	/**
	 * 创建一个工具条的分隔符
	 * 
	 * @return
	 */
	private JToolBar.Separator createSeparator() {
		JToolBar.Separator separator = new JToolBar.Separator();
		separator.setOrientation(SwingConstants.VERTICAL);
		return separator;
	}

	/**
	 * 用数据集全名匹配默认数据集名
	 * 
	 * @param wholeName
	 *            数据集名
	 * @param typeName
	 *            默认数据集名
	 * @return 符合默认命名格式，返回true
	 */
	private boolean testDefaultName(String wholeName, String typeName) {
		boolean result = false;
		if (wholeName.equalsIgnoreCase(typeName)) {
			result = true;
		} else if (wholeName.indexOf(typeName) == 0) {
			String leftedString = wholeName.substring(typeName.length(), wholeName.length());
			// 如果是 "_" 开头，则去掉下划线
			if (leftedString.startsWith("_")) {
				leftedString = leftedString.substring(1, leftedString.length());
			}

			if (Integer.valueOf(leftedString) >= 0) {
				result = true;
			}
		}

		return result;
	}

	/**
	 * 判断输入是否的数据集名称是否符合规范
	 * 
	 * @param datasetName
	 * @return
	 */
	private boolean isAviliableName(String datasetName) {
		boolean flag = false;
		char c = datasetName.charAt(0);
		if ('_' == c || ('0' < c && c < '9')) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	/**
	 * 检索是否是默认指定的名字
	 * 
	 * @param datasetName
	 *            被检索的数据集名
	 * @return 符合默认命名格式，返回true
	 */
	private boolean isDefaultName(String datasetName) {
		boolean result = false;
		String defaultTypeName = "";

		if (datasetName.indexOf("New_Point3D") == 0) {
			defaultTypeName = "New_Point3D";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Line3D") == 0) {
			defaultTypeName = "New_Line3D";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Region3D") == 0) {
			defaultTypeName = "New_Region3D";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Text") == 0) {
			defaultTypeName = "New_Text";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_CAD") == 0) {
			defaultTypeName = "New_CAD";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Tabular") == 0) {
			defaultTypeName = "New_Tabular";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Template") == 0) {
			defaultTypeName = "New_Template";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Point") == 0) {
			defaultTypeName = "New_Point";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Line") == 0) {
			defaultTypeName = "New_Line";
			result = testDefaultName(datasetName, defaultTypeName);
		} else if (datasetName.indexOf("New_Region") == 0) {
			defaultTypeName = "New_Region";
			result = testDefaultName(datasetName, defaultTypeName);
		}
		return result;
	}

	private String getDatasetName(int rowIndex) {
		String datasetName = "";
		try {
			if (this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_DatasetName) != null) {
				datasetName = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_DatasetName).toString();

				if (datasetName != null && this.isDefaultName(datasetName)) {
					DatasetType datasetType = CommonToolkit.DatasetTypeWrap.findType(this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_DatasetType)
							.toString());

					datasetName = this.getDefaultDatasetName(datasetType);
					String datasourceName = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource).toString();
					Datasource datasource = Application.getActiveApplication().getWorkspace().getDatasources().get(datasourceName);
					datasetName = CommonToolkit.DatasetWrap.getAvailableDatasetName(datasource, datasetName, this.getAllDatasetNames(datasource, rowIndex));
				}
			}

		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return datasetName;
	}

	private String[] getAllDatasetNames(Datasource datasource, int rowIndex) {
		ArrayList<String> datasetNames = new ArrayList<String>();
		try {
			if (datasource != null) {
				for (int index = 0; index < this.table.getRowCount(); index++) {
					if (this.table.getModel().getValueAt(index, COLUMN_INDEX_TargetDatasource) != null
							&& datasource.getAlias().equals(this.table.getModel().getValueAt(index, COLUMN_INDEX_TargetDatasource).toString())) {
						datasetNames.add(this.table.getModel().getValueAt(index, COLUMN_INDEX_DatasetName).toString().toLowerCase());
					}
				}
			} else {
				for (int index = 0; index < this.table.getRowCount(); index++) {
					if (index != rowIndex && this.table.getModel().getValueAt(index, COLUMN_INDEX_TargetDatasource) != null
							&& this.table.getModel().getValueAt(index, COLUMN_INDEX_TargetDatasource).toString() != "") {
						datasetNames.add(this.table.getModel().getValueAt(index, COLUMN_INDEX_DatasetName).toString().toLowerCase());
					}
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}

		return datasetNames.toArray(new String[datasetNames.size()]);
	}

	private void table_ValueChanged(TableModelEvent e) {
		String beforeName = "";
		isDatasetTypeValueChanged = false;
		Workspace workspace = Application.getActiveApplication().getWorkspace();
		try {
			int rowIndex = e.getFirstRow();
			int columnIndex = e.getColumn();
			// 如果是编辑数据集名称
			if (e.getColumn() == COLUMN_INDEX_DatasetName) {
				if (this.table.getModel().getValueAt(rowIndex, columnIndex) != null) {
					String datasetName = table.getModel().getValueAt(rowIndex, columnIndex).toString();
					String datasourceName = this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource).toString();
					if (this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource) != null) {
						Datasource datasource = workspace.getDatasources().get(datasourceName);
						if (!datasource.getDatasets().isAvailableDatasetName(datasetName) || !isAviliableName(datasetName)) {
							datasetName = "";
						}
						if ("" == datasetName) {
							beforeName = defaultDatasetName;
							this.table.getModel().setValueAt(beforeName, rowIndex, columnIndex);
						}
					}

					if (datasetName.length() > 0 && rowIndex == this.table.getRowCount() - 1
							&& this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource) != null
							&& this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource).toString().length() > 0 && !isDatasetTypeValueChanged) {
						this.copyPreDatasetInfo(rowIndex);
						this.buttonDelete.setEnabled(true);
					}
				}
			} else if (e.getColumn() == COLUMN_INDEX_DatasetType) { // 如果是编辑数据集类型
				isDatasetTypeValueChanged = true;

				// 增加切换最后一行的数据集类型时 自动增加一行，当前行默认给一个名字
				String datasetName = this.getDatasetName(rowIndex);
				if (datasetName == "") {
					// 如果是最后一行的操作 先复制一行
					if (rowIndex == this.table.getRowCount() - 1) {
						this.copyPreDatasetInfo(rowIndex);
						this.buttonDelete.setEnabled(true);
					}
					Datasource datasource = workspace.getDatasources()
							.get(this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_TargetDatasource).toString());

					DatasetType datasetType = CommonToolkit.DatasetTypeWrap.findType(this.table.getModel().getValueAt(rowIndex, COLUMN_INDEX_DatasetType)
							.toString());

					datasetName = CommonToolkit.DatasetWrap.getAvailableDatasetName(datasource, this.getDefaultDatasetName(datasetType),
							getAllDatasetNames(datasource, rowIndex));
				}

				this.table.getModel().setValueAt(datasetName, rowIndex, COLUMN_INDEX_DatasetName);
				String typeName = this.table.getValueAt(rowIndex, COLUMN_INDEX_DatasetType).toString();
				DatasetType datasetType = CommonToolkit.DatasetTypeWrap.findType(typeName);
				if (datasetType != DatasetType.LINE && datasetType != DatasetType.REGION) {
					this.table.getModel().setValueAt(CommonProperties.getString("String_EncodeType_None"), rowIndex, COLUMN_INDEX_EncodeType);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void buttonSelectAll_Click() {
		try {
			ListSelectionModel selectionModel = this.table.getSelectionModel();
			selectionModel.addSelectionInterval(0, this.table.getRowCount() - 1);
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void buttonSelectInvert_Click() {
		try {
			int[] temp = this.table.getSelectedRows();
			ArrayList<Integer> selectedRows = new ArrayList<Integer>();
			for (int index = 0; index < temp.length; index++) {
				selectedRows.add(temp[index]);
			}

			ListSelectionModel selectionModel = table.getSelectionModel();
			selectionModel.clearSelection();
			for (int index = 0; index < this.table.getRowCount(); index++) {
				if (!selectedRows.contains(index)) {
					selectionModel.addSelectionInterval(index, index);
				}
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void buttonDelete_Click() {
		try {
			boolean isResetIndex = false;
			Boolean isLastValidateItem = false;
			int[] selectedRows = this.table.getSelectedRows();
			for (int index = selectedRows.length - 1; index >= 0; index--) {
				if (selectedRows[index] != this.table.getRowCount() - 1) {
					if (selectedRows[index] == this.table.getRowCount() - 2) {
						isLastValidateItem = true;
					}
					this.table.removeRow(selectedRows[index]);
					isResetIndex = true;
				}
			}

			if (isResetIndex) {
				for (int j = 0; j < this.table.getRowCount(); j++) {
					this.table.getModel().setValueAt(j + 1, j, 0);
				}
			}

			if (isLastValidateItem) {
				ListSelectionModel selectionModel = table.getSelectionModel();
				selectionModel.addSelectionInterval(this.table.getRowCount() - 2, this.table.getRowCount() - 2);
				selectionModel.removeIndexInterval(this.table.getRowCount() - 1, this.table.getRowCount() - 1);
			}
			if (1 == table.getRowCount()) {
				this.buttonDelete.setEnabled(false);
			} else {
				this.buttonDelete.setEnabled(true);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void buttonSetting_Click() {
		try {
			UICommonToolkit.showMessageDialog("Nothing");
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void okButton_Click() {
		try {
			if (this.chckbxAutoClose.isSelected()) {
				this.dispose();
			}

			this.dialogResult = DialogResult.OK;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	private void cancelButton_Click() {
		try {
			this.setVisible(false);
			this.dialogResult = DialogResult.CANCEL;
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}
}
