package com.supermap.desktop.util;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.supermap.data.Dataset;
import com.supermap.data.conversion.DataExport;
import com.supermap.data.conversion.ExportResult;
import com.supermap.data.conversion.ExportSetting;
import com.supermap.data.conversion.ExportSettings;
import com.supermap.data.conversion.ExportSteppedEvent;
import com.supermap.data.conversion.ExportSteppedListener;
import com.supermap.data.conversion.FileType;
import com.supermap.desktop.Application;
import com.supermap.desktop.ExportFileInfo;
import com.supermap.desktop.dataconversion.DataConversionProperties;
import com.supermap.desktop.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.ui.UICommonToolkit;

public class DataExportCallable extends UpdateProgressCallable {
	private ArrayList<ExportFileInfo> exports;
	private JTable exportTable;
	/**
	 * 强制覆盖是否被选中
	 */
	private boolean isCoverChoosed;
	
	/**
	 * 是否强制覆盖
	 */
	private boolean isCover;
	public DataExportCallable(List<ExportFileInfo> exports, JTable exportTable,boolean isCoverChoosed, boolean isCover) {
		this.exportTable = exportTable;
		this.exports = (ArrayList<ExportFileInfo>) exports;
		this.isCoverChoosed = isCoverChoosed;
		this.isCover = isCover;
	}

	private String getDatasetAlis(ExportSetting tempSetting) {
		Dataset tempDataset = (Dataset) tempSetting.getSourceData();
		return tempDataset.getName() + DataConversionProperties.getString("string_index_and") + tempDataset.getDatasource().getAlias();
	}

	@Override
	public Boolean call() throws Exception {
		try {
			for (int i = 0; i < exports.size(); i++) {
				ExportFileInfo export = exports.get(i);
				ExportSetting tempExportSetting = export.getExportSetting();
				Dataset dataset = export.getDataset();
				String filePath = "";
				if (FileType.TEMSClutter == export.getTargetFileType()) {
					filePath = export.getFilepath() + export.getFileName() + DataConversionProperties.getString("string_index_pause") + "b";
				} else {
					filePath = export.getFilepath() + export.getFileName() + DataConversionProperties.getString("string_index_pause")
							+ export.getTargetFileType();
				}
				File tempFile = new File(filePath);
				if (tempFile.exists()) {
					if(isCoverChoosed){
						tempExportSetting.setOverwrite(isCover);
					} else if (UICommonToolkit
							.showConfirmDialog(String.format(DataConversionProperties.getString("String_Message_OverWriteFile"), tempFile.getName())) == JOptionPane.OK_OPTION) {
						tempExportSetting.setOverwrite(true);
					}
				}
				tempExportSetting.setSourceData(dataset);
				tempExportSetting.setTargetFileType(export.getTargetFileType());
				tempExportSetting.setTargetFilePath(filePath);
				final DataExport dataExport = new DataExport();
				ExportSettings exportSettings = dataExport.getExportSettings();
				exportSettings.add(tempExportSetting);
				PercentProgress progress = new PercentProgress(i);
				String time = "";
				dataExport.addExportSteppedListener(progress);
				ExportResult result = dataExport.run();
				printExportInfo(result, i, time);
			}

		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return true;
	}

	/**
	 * 进度事件得到运行时间
	 * 
	 * @author Administrator
	 *
	 */
	class PercentProgress implements ExportSteppedListener {
		private int i;

		public PercentProgress(int i) {
			this.i = i;
		}

		@Override
		public void stepped(ExportSteppedEvent arg0) {
			try {
				int count = exports.size();
				int totalPercent = (int) (((i + 0.0) / count) * 100);
				updateProgressTotal(
						arg0.getSubPercent(),
						totalPercent,
						MessageFormat.format(DataConversionProperties.getString("String_ProgressControl_TotalImportProgress"),
								String.valueOf(totalPercent), String.valueOf(exports.size())),
						MessageFormat.format(DataConversionProperties.getString("String_ProgressControl_SubImportProgress"),
								String.valueOf(arg0.getSubPercent())));
			} catch (CancellationException e) {
				arg0.setCancel(true);
			}
		}
	}

	/**
	 * 打印导出信息
	 * 
	 * @param result
	 */
	private void printExportInfo(ExportResult result, int i, String time) {
		try {
			if (null != result) {
				String successExportInfo = DataConversionProperties.getString("String_FormExport_OutPutInfoTwo");
				String failExportInfo = DataConversionProperties.getString("String_FormExport_OutPutInfoOne");
				ExportSetting[] successExportSettings = result.getSucceedSettings();
				ExportSetting[] failExportSettings = result.getFailedSettings();

				if (null != successExportSettings && 0 < successExportSettings.length) {
					exports.get(i).setState(DataConversionProperties.getString("String_FormImport_Succeed"));
					String successDatasetAlis = getDatasetAlis(successExportSettings[0]);
					Application.getActiveApplication().getOutput()
							.output(MessageFormat.format(successExportInfo, successDatasetAlis, successExportSettings[0].getTargetFilePath(), time));
				} else if (null != failExportSettings && 0 < failExportSettings.length) {
					exports.get(i).setState(DataConversionProperties.getString("String_FormImport_NotSucceed"));
					String failDatasetAlis = getDatasetAlis(failExportSettings[0]);
					Application.getActiveApplication().getOutput()
							.output(MessageFormat.format(failExportInfo, failDatasetAlis));
				}

				// 刷新table
				((ExportModel) exportTable.getModel()).updateRows(exports);
			} else {
				Application.getActiveApplication().getOutput().output(DataConversionProperties.getString("string_exporterror"));
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}
}