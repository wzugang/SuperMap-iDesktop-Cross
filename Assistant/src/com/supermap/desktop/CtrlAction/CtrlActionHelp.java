package com.supermap.desktop.CtrlAction;

import java.io.IOException;

import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.ICtrlAction;
import com.supermap.desktop.Interface.IDockbar;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMain;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.ui.controls.DockbarManager;

public class CtrlActionHelp extends CtrlAction {
	
	public CtrlActionHelp(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		try {
			Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler http://www.supermap.com/cn/");
		} catch (IOException e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}
}
