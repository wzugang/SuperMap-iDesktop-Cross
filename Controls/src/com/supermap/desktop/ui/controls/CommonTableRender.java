package com.supermap.desktop.ui.controls;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CommonTableRender implements TableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value instanceof JPanel) {
			if (isSelected) {// 设置选中时的背景色
				((JPanel) value).setBackground(Color.LIGHT_GRAY);
			} else {
				((JPanel) value).setBackground(Color.white);
			}
			return (JPanel) value;
		}
		return null;
	}

}
