package com.supermap.desktop.core;

import java.net.URL;
import java.util.Enumeration;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.supermap.desktop.Application;

public class CoreActivator implements BundleActivator {

	ServiceRegistration<?> registration;
	CoreServiceTracker serviceTracker;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Hello SuperMap === Core!!");

		// 不知道为什么，core会被加载两次，暂时先这么处理
		if (Application.getActiveApplication() == null || Application.getActiveApplication().getPluginManager().getBundle("SuperMap.Desktop.Core") == null) {
			this.serviceTracker = new CoreServiceTracker(context);
			this.serviceTracker.open();
			this.registration = context.registerService(Application.class.getName(), new Application(), null);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye SuperMap === Core!!");

		this.serviceTracker.close();
		this.registration.unregister();
	}

}
