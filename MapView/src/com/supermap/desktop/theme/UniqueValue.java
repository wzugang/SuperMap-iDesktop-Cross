package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 包含左Checkbox右Panel的类
 * 
 * @author zhaosy
 * 
 */
public class UniqueValue extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JCheckBox checkBox;
	private Color color;
	private boolean isFlag;

	public UniqueValue() {
		super();
		this.setLayout(new GridLayout(1, 1));
		add(getcheckBox());
		add(getLabel());
	}

	/**
	 * 颜色Label
	 * 
	 * @return
	 */
	public JLabel getLabel() {
		if (label == null) {
			label = new JLabel();
		}
		return label;
	}

	/**
	 * 显示选择
	 * 
	 * @return
	 */
	public JCheckBox getcheckBox() {
		if (checkBox == null) {
			checkBox = new JCheckBox();
			checkBox.setSelected(true);
			checkBox.addMouseListener(new MouseAdapter() {

				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == 1 && e.getClickCount() == 1) {
						checkBox.setSelected(!checkBox.isSelected());
					}
				}
			});
		}
		return checkBox;
	}

	/**
	 * 返回內部panel的顏色
	 * 
	 * @return m_color
	 */
	public Color getColor() {
		color = label.getBackground();
		return color;
	}

	/**
	 * 设置颜色
	 * 
	 * @param color
	 */
	public void setColor(Color color) {
		label.setBackground(color);

	}

	/**
	 * 选择框是否被选中
	 * 
	 * @return m_isFlag
	 */
	public boolean isFlag() {
		isFlag = checkBox.isSelected();
		return isFlag;
	}

	/**
	 * 设置选择框的选中状态
	 * 
	 * @param isFlag
	 */
	public void setFlag(boolean isFlag) {
		checkBox.setSelected(isFlag);
	}
}
