package com.supermap.desktop.theme;

import java.util.ArrayList;

import com.supermap.desktop.Application;

/**
 * 专题图类型的相关信息
 * 
 * @author WDY
 */
public abstract class ThemeInfo {
	protected ArrayList<Class<?>> list;

	// 各个面板的名称，用于导航条
	public String[] getPanelNames() {
		int size = list.size();
		String[] strings = new String[size];
		Object name = null;

		for (int i = 0; i < size; i++) {
			try {
				name = list.get(i).getDeclaredMethod("getPanelName", null).invoke(null, null);
			} catch (Exception e) {
				Application.getActiveApplication().getOutput().output(e);
			}

			strings[i] = name.toString();
		}

		return strings;
	}

	// 某个专题图所需所有面板
	public ArrayList<Class<?>> getPanels() {

		return list;
	}
}
