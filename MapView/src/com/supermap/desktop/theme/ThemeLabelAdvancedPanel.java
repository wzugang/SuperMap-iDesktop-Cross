package com.supermap.desktop.theme;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;

import com.supermap.data.DatasetType;
import com.supermap.data.Size2D;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.SymbolPreViewPanel;
import com.supermap.mapping.AlongLineDirection;
import com.supermap.mapping.OverLengthLabelMode;
import com.supermap.mapping.Theme;
import com.supermap.mapping.ThemeLabel;

/**
 * @author WDY
 */
class ThemeLabelAdvancedPanel extends ThemeChangePanel {

	private static final long serialVersionUID = 1L;
	private JSpinner jSpinnerLongLineSpaceRatio;
	private JSpinner jSpinnerSingleMaxLabelLength;

	private JComboBox<String> jComboBoxAlongLineDirection;
	private JComboBox<String> jComboBoxOverLengthLabelMode;

	private JTextField jTextFieldRepeatInterval = new JTextField();
	private JTextField jTextFieldMinTextWidth = new JTextField();
	private JTextField jTextFieldMaxTextWidth = new JTextField();
	private JTextField jTextFieldTextExtentInflationX = new JTextField();
	private JTextField jTextFieldTextExtentInflationY = new JTextField();
	private JTextField jTextFieldMaxTextHeight = new JTextField();
	private JTextField jTextFieldMinTextHeight = new JTextField();

	private JCheckBox jCheckBoxAngleFixed = new JCheckBox();
	private JCheckBox jCheckBoxAlongLine = new JCheckBox();
	private JCheckBox jCheckBoxRepeatIntervalFixed = new JCheckBox();

	private JPanel jPanelText;
	private JPanel jPanelAlongLine;
	private SymbolPreViewPanel jPanelPreView;
	private JPanel jPanelHeight;

	private JLabel jLabelAlongLineDirection = new JLabel();
	private JLabel jLabelAlongLineSpaceRatio = new JLabel();
	private JLabel jLabelRepeatInterval = new JLabel();
	private JLabel overLengthLabel = new JLabel();
	private JLabel maxLabelLength = new JLabel();
	private JLabel textExtentInflationX = new JLabel();
	private JLabel textExtentInflationY = new JLabel();
	private JLabel maxTextHeight = new JLabel();
	private JLabel minTextHeight = new JLabel();
	private JLabel maxTextWidth = new JLabel();
	private JLabel minTextWidth = new JLabel();

	private ThemeLabel currentTheme;
	private Size2D size2D;

	private double width;
	private double heigth;

	public ThemeLabelAdvancedPanel(SymbolPreViewPanel previewPanel, Theme themeLabel) {
		super();
		setLayout(null);
		this.setBounds(5, 35, 545, 325);
		this.jPanelPreView = previewPanel;
		this.currentTheme = (ThemeLabel) themeLabel;

		width = currentTheme.getTextExtentInflation().getWidth();
		heigth = currentTheme.getTextExtentInflation().getHeight();

		initResources();
		getTextPanel();
		getLayyerViewPanel();
		getFontHeightPanel();
		getRightDownPanel();
		setTheme(currentTheme);
	}

	public static String getPanelName() {
		return MapViewProperties.getString("String_PanelTitle_AdvancedSet");
	}

	private void initResources() {
		overLengthLabel.setText(MapViewProperties.getString("String_OverLengthLabelMode"));
		maxLabelLength.setText(MapViewProperties.getString("String_CharCount"));
		textExtentInflationX.setText(MapViewProperties.getString("String_TextAvoidCrossWiseBuffer"));
		textExtentInflationY.setText(MapViewProperties.getString("String_TextAvoidPortraitBuffer"));
		jCheckBoxAlongLine.setText(MapViewProperties.getString("String_AlongLine"));
		jCheckBoxAngleFixed.setText(MapViewProperties.getString("String_CheckBox_IsTextAngleFixed"));
		jLabelAlongLineDirection.setText(MapViewProperties.getString("String_LineDirection"));
		jLabelAlongLineSpaceRatio.setText(MapViewProperties.getString("String_SpaceRatio"));
		jLabelRepeatInterval.setText(MapViewProperties.getString("String_RepeatInterval"));
		jCheckBoxRepeatIntervalFixed.setText(MapViewProperties.getString("String_RepeatIntervalFixed"));
		maxTextHeight.setText(MapViewProperties.getString("String_MaxHeight"));
		minTextHeight.setText(MapViewProperties.getString("String_MinHeight"));
		maxTextWidth.setText(MapViewProperties.getString("String_MaxWidth"));
		minTextWidth.setText(MapViewProperties.getString("String_MinWidth"));

		jComboBoxOverLengthLabelMode = new JComboBox<String>(new String[] { MapViewProperties.getString("String_ColorTable_Default"),
				MapViewProperties.getString("String_ChangeColumn"), MapViewProperties.getString("String_IgnoreBeyound") });
		jComboBoxAlongLineDirection = new JComboBox<String>(new String[] { MapViewProperties.getString("String_AlongLineDirection_LeftTopToRightBottom"),
				MapViewProperties.getString("String_AlongLineDirection_LeftBottomToRightTop"),
				MapViewProperties.getString("String_AlongLineDirection_RightBottomToLeftTop"),
				MapViewProperties.getString("String_AlongLineDirection_RightTopToLeftBottom"),
				MapViewProperties.getString("String_AlongLineDirection_AlongLineNormal") });
	}

	private JPanel getTextPanel() {
		if (jPanelText == null) {
			jPanelText = new JPanel();
			jPanelText.setLayout(null);
			jPanelText.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, false));
			jPanelText.setBounds(0, 18, 250, 106);

			overLengthLabel.setHorizontalAlignment(SwingConstants.LEFT);
			overLengthLabel.setBounds(10, 5, 88, 20);
			jPanelText.add(overLengthLabel);

			maxLabelLength.setHorizontalAlignment(SwingConstants.LEFT);
			maxLabelLength.setBounds(10, 30, 88, 18);
			jPanelText.add(maxLabelLength);

			jComboBoxOverLengthLabelMode.setBounds(125, 5, 115, 20);
			jComboBoxOverLengthLabelMode.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					OverLengthLabelMode overLengthLabelModeValue = null;
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						if (jComboBoxOverLengthLabelMode.getSelectedIndex() == 0) {
							overLengthLabelModeValue = OverLengthLabelMode.NONE;
							if (jSpinnerSingleMaxLabelLength.isEnabled()) {
								jSpinnerSingleMaxLabelLength.setEnabled(false);
							}
						} else if (jComboBoxOverLengthLabelMode.getSelectedIndex() == 1) {
							overLengthLabelModeValue = OverLengthLabelMode.NEWLINE;
							if (!jSpinnerSingleMaxLabelLength.isEnabled()) {
								jSpinnerSingleMaxLabelLength.setEnabled(true);
							}
						} else if (jComboBoxOverLengthLabelMode.getSelectedIndex() == 2) {
							overLengthLabelModeValue = OverLengthLabelMode.OMIT;
							if (!jSpinnerSingleMaxLabelLength.isEnabled()) {
								jSpinnerSingleMaxLabelLength.setEnabled(true);
							}
						}
						currentTheme.setOverLengthMode(overLengthLabelModeValue);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jPanelText.add(jComboBoxOverLengthLabelMode);

			jSpinnerSingleMaxLabelLength = new JSpinner();
			jSpinnerSingleMaxLabelLength.setBounds(125, 30, 115, 20);
			SpinnerNumberModel model = new SpinnerNumberModel(1, 1, 10000, 1);
			jSpinnerSingleMaxLabelLength.setModel(model);
			NumberEditor numberEditor = (NumberEditor) jSpinnerSingleMaxLabelLength.getEditor();
			numberEditor.getFormat().applyPattern("0.#");
			JFormattedTextField textField = numberEditor.getTextField();
			textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
			NumberFormatter formatter = (NumberFormatter) textField.getFormatter();
			formatter.setAllowsInvalid(false);
			textField.getDocument().addDocumentListener(new DocumentListener() {

				@Override
				public void insertUpdate(DocumentEvent e) {
					NumberEditor numberEditor = (NumberEditor) jSpinnerSingleMaxLabelLength.getEditor();
					numberEditor.getFormat().applyPattern("0.#");
					JFormattedTextField textField = numberEditor.getTextField();
					currentTheme.setMaxLabelLength(Integer.valueOf(textField.getText()));
					jPanelPreView.refreshPreViewMapControl(currentTheme);
				}

				@Override
				public void changedUpdate(DocumentEvent e) {
					// nothing
				}

				@Override
				public void removeUpdate(DocumentEvent e) {
					// nothing
				}
			});
			jPanelText.add(jSpinnerSingleMaxLabelLength);
			add(jPanelText);

			jTextFieldTextExtentInflationX.setBounds(125, 55, 115, 18);
			jPanelText.add(jTextFieldTextExtentInflationX);
			jTextFieldTextExtentInflationX.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				@Override
				public void changedUpdate(DocumentEvent e) {
					// nothing
				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					try {
						width = Integer.valueOf(jTextFieldTextExtentInflationX.getText());
						isException = false;
						// 处理各种情况
						if (width < 0
								|| (jTextFieldTextExtentInflationX.getText().indexOf("0") == 0 && jTextFieldTextExtentInflationX.getText().length() > 1 || jTextFieldTextExtentInflationX
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldTextExtentInflationX.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								jTextFieldTextExtentInflationX.setText(record);
							}
						});
					}
					if (isException == false) {
						if (size2D == null) {
							size2D = new Size2D(width, heigth);
						} else {
							size2D.setWidth(width);
						}
						currentTheme.setTextExtentInflation(size2D);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				@Override
				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldTextExtentInflationX.getText())) {
							return;
						}
						if (!jTextFieldTextExtentInflationX.getText().equals(record)) {
							record = jTextFieldTextExtentInflationX.getText();
						}
						size2D.setWidth(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldTextExtentInflationX.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldTextExtentInflationX.getText().trim())
							|| (jTextFieldTextExtentInflationX.getText().indexOf("0") == 0 && jTextFieldTextExtentInflationX.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								jTextFieldTextExtentInflationX.setText("0");
							}
						});
					}
				}
			});
			jTextFieldTextExtentInflationX.setHorizontalAlignment(JTextField.RIGHT);

			jTextFieldTextExtentInflationY.setBounds(125, 80, 115, 18);
			jPanelText.add(jTextFieldTextExtentInflationY);
			jTextFieldTextExtentInflationY.setHorizontalAlignment(JTextField.RIGHT);
			jTextFieldTextExtentInflationY.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					try {
						heigth = Integer.valueOf(jTextFieldTextExtentInflationY.getText());
						isException = false;
						// 处理各种情况
						if (heigth < 0
								|| (jTextFieldTextExtentInflationY.getText().indexOf("0") == 0 && jTextFieldTextExtentInflationY.getText().length() > 1 || jTextFieldTextExtentInflationY
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldTextExtentInflationY.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldTextExtentInflationY.setText(record);
							}
						});
					}
					if (isException == false) {
						if (size2D == null) {
							size2D = new Size2D(width, heigth);
						} else {
							size2D.setHeight(heigth);
						}
						currentTheme.setTextExtentInflation(size2D);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldTextExtentInflationY.getText())) {
							return;
						}
						if (!jTextFieldTextExtentInflationY.getText().equals(record)) {
							record = jTextFieldTextExtentInflationY.getText();
						}
						size2D.setHeight(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldTextExtentInflationY.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldTextExtentInflationY.getText().trim())
							|| (jTextFieldTextExtentInflationY.getText().indexOf("0") == 0 && jTextFieldTextExtentInflationY.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldTextExtentInflationY.setText("0");
							}
						});
					}
				}
			});

			textExtentInflationX.setBounds(10, 55, 108, 20);
			jPanelText.add(textExtentInflationX);
			textExtentInflationX.setHorizontalAlignment(SwingConstants.CENTER);

			textExtentInflationY.setBounds(10, 80, 108, 18);
			jPanelText.add(textExtentInflationY);
			textExtentInflationY.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jPanelText;
	}

	private JPanel getRightDownPanel() {
		if (jPanelAlongLine == null) {
			jPanelAlongLine = new JPanel();
			jPanelAlongLine.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY, 1, false), "    ", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, null, null));
			jPanelAlongLine.setLayout(null);
			jPanelAlongLine.setBounds(250, 127, 275, 200);

			jCheckBoxAlongLine.setBounds(10, 4, 77, 13);
			jCheckBoxAlongLine.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					boolean isAlongLineValue = jCheckBoxAlongLine.isSelected();
					if (isAlongLineValue == false) {
						jCheckBoxAngleFixed.setEnabled(false);
						if (!jCheckBoxAngleFixed.isSelected()) {
							jComboBoxAlongLineDirection.setEnabled(false);
							jLabelAlongLineDirection.setEnabled(false);
						}
						jSpinnerLongLineSpaceRatio.setEnabled(false);
						jLabelAlongLineSpaceRatio.setEnabled(false);
						jTextFieldRepeatInterval.setEnabled(false);
						jLabelRepeatInterval.setEnabled(false);
						jCheckBoxRepeatIntervalFixed.setEnabled(false);
					} else {
						jCheckBoxAngleFixed.setEnabled(true);
						if (!jCheckBoxAngleFixed.isSelected()) {
							jComboBoxAlongLineDirection.setEnabled(true);
							jLabelAlongLineDirection.setEnabled(true);
						}
						jSpinnerLongLineSpaceRatio.setEnabled(true);
						jLabelAlongLineSpaceRatio.setEnabled(true);
						jTextFieldRepeatInterval.setEnabled(true);
						jLabelRepeatInterval.setEnabled(true);
						jCheckBoxRepeatIntervalFixed.setEnabled(true);

						// 初始化沿线周期间距
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								int value = (int) (currentTheme.getLabelRepeatInterval() * 10);
								jTextFieldRepeatInterval.setText(String.valueOf(value));
							}
						});
					}
					currentTheme.setAlongLine(isAlongLineValue);
					jPanelPreView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelAlongLine.add(jCheckBoxAlongLine);

			jCheckBoxAngleFixed.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					boolean isAngleFixedValue = jCheckBoxAngleFixed.isSelected();
					if (isAngleFixedValue == true) {
						jComboBoxAlongLineDirection.setEnabled(false);
						jLabelAlongLineDirection.setEnabled(false);
					} else {
						jComboBoxAlongLineDirection.setEnabled(true);
						jLabelAlongLineDirection.setEnabled(true);
					}
					currentTheme.setAngleFixed(isAngleFixedValue);
					jPanelPreView.refreshPreViewMapControl(currentTheme);
				}
			});
			jCheckBoxAngleFixed.setBounds(10, 30, 103, 26);
			jPanelAlongLine.add(jCheckBoxAngleFixed);

			jLabelAlongLineDirection.setBounds(15, 65, 89, 18);
			jPanelAlongLine.add(jLabelAlongLineDirection);

			jLabelAlongLineSpaceRatio.setBounds(15, 100, 89, 18);
			jPanelAlongLine.add(jLabelAlongLineSpaceRatio);

			jLabelRepeatInterval.setBounds(15, 135, 89, 18);
			jPanelAlongLine.add(jLabelRepeatInterval);

			jTextFieldRepeatInterval.setHorizontalAlignment(JTextField.RIGHT);
			jTextFieldRepeatInterval.setBounds(110, 135, 124, 18);
			jTextFieldRepeatInterval.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						value = Integer.valueOf(jTextFieldRepeatInterval.getText());
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (jTextFieldRepeatInterval.getText().indexOf("0") == 0 && jTextFieldRepeatInterval.getText().length() > 1 || jTextFieldRepeatInterval
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = String.valueOf(value);
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldRepeatInterval.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setLabelRepeatInterval(value * 0.1);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldRepeatInterval.getText())) {
							return;
						}
						if (!jTextFieldRepeatInterval.getText().equals(record)) {
							record = jTextFieldRepeatInterval.getText();
						}
						currentTheme.setLabelRepeatInterval(Integer.valueOf(record) * 0.1);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldRepeatInterval.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldRepeatInterval.getText().trim())
							|| (jTextFieldRepeatInterval.getText().indexOf("0") == 0 && jTextFieldRepeatInterval.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldRepeatInterval.setText("0");
							}
						});
					}
				}
			});
			jPanelAlongLine.add(jTextFieldRepeatInterval);

			jComboBoxAlongLineDirection.setBounds(110, 65, 124, 20);
			jComboBoxAlongLineDirection.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() != ItemEvent.DESELECTED) {
						AlongLineDirection alongLineDirection = null;
						if (jComboBoxAlongLineDirection.getSelectedIndex() == 0) {
							alongLineDirection = AlongLineDirection.LEFT_TOP_TO_RIGHT_BOTTOM;
						} else if (jComboBoxAlongLineDirection.getSelectedIndex() == 1) {
							alongLineDirection = AlongLineDirection.LEFT_BOTTOM_TO_RIGHT_TOP;
						} else if (jComboBoxAlongLineDirection.getSelectedIndex() == 2) {
							alongLineDirection = AlongLineDirection.RIGHT_BOTTOM_TO_LEFT_TOP;
						} else if (jComboBoxAlongLineDirection.getSelectedIndex() == 3) {
							alongLineDirection = AlongLineDirection.RIGHT_TOP_TO_LEFT_BOTTOM;
						} else if (jComboBoxAlongLineDirection.getSelectedIndex() == 4) {
							alongLineDirection = AlongLineDirection.ALONG_LINE_NORMAL;
						}
						currentTheme.setAlongLineDirection(alongLineDirection);
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jPanelAlongLine.add(jComboBoxAlongLineDirection);

			jSpinnerLongLineSpaceRatio = new JSpinner();
			jSpinnerLongLineSpaceRatio.setBounds(110, 100, 124, 18);
			// 初始化
			jSpinnerLongLineSpaceRatio.setEnabled(jCheckBoxAlongLine.isSelected());
			jLabelAlongLineSpaceRatio.setEnabled(jCheckBoxAlongLine.isSelected());
			SpinnerNumberModel model = new SpinnerNumberModel(1, 1, 10000, 1);
			jSpinnerLongLineSpaceRatio.setModel(model);

			NumberEditor numberEditor = (NumberEditor) jSpinnerLongLineSpaceRatio.getEditor();
			numberEditor.getFormat().applyPattern("0.#");
			JFormattedTextField textField = numberEditor.getTextField();
			textField.setFocusLostBehavior(JFormattedTextField.COMMIT);
			NumberFormatter formatter = (NumberFormatter) textField.getFormatter();
			formatter.setAllowsInvalid(false);

			textField.getDocument().addDocumentListener(new DocumentListener() {
				public void insertUpdate(DocumentEvent e) {
					currentTheme.setAlongLineSpaceRatio(Integer.valueOf(jSpinnerLongLineSpaceRatio.getModel().getValue().toString()));
					jPanelPreView.refreshPreViewMapControl(currentTheme);

				}

				public void changedUpdate(DocumentEvent e) {
				}

				public void removeUpdate(DocumentEvent e) {
				}
			});

			jPanelAlongLine.add(jSpinnerLongLineSpaceRatio);
			add(jPanelAlongLine);

			jCheckBoxRepeatIntervalFixed.setBounds(10, 160, 142, 26);
			jCheckBoxRepeatIntervalFixed.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					currentTheme.setRepeatIntervalFixed(jCheckBoxRepeatIntervalFixed.isSelected());
					jPanelPreView.refreshPreViewMapControl(currentTheme);
				}
			});
			jPanelAlongLine.add(jCheckBoxRepeatIntervalFixed);

			final JLabel label_1_1 = new JLabel();
			label_1_1.setText("0.1mm");
			label_1_1.setBounds(234, 138, 40, 12);
			label_1_1.setFont(new Font("0.1mm", Font.ITALIC, 10));
			jPanelAlongLine.add(label_1_1);
		}
		return jPanelAlongLine;
	}

	private SymbolPreViewPanel getLayyerViewPanel() {
		jPanelPreView.setBounds(0, 127, 250, 200);
		add(jPanelPreView);
		return jPanelPreView;
	}

	private JPanel getFontHeightPanel() {
		if (jPanelHeight == null) {
			jPanelHeight = new JPanel();
			jPanelHeight.setLayout(null);
			jPanelHeight.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, false));
			jPanelHeight.setBounds(251, 18, 275, 106);
			add(jPanelHeight);

			maxTextHeight.setHorizontalAlignment(SwingConstants.CENTER);
			maxTextHeight.setBounds(10, 5, 81, 20);
			jPanelHeight.add(maxTextHeight);

			minTextHeight.setHorizontalAlignment(SwingConstants.CENTER);
			minTextHeight.setBounds(10, 30, 81, 20);
			jPanelHeight.add(minTextHeight);

			jTextFieldMinTextHeight = new JTextField();
			jTextFieldMinTextHeight.setHorizontalAlignment(JTextField.RIGHT);
			jTextFieldMinTextHeight.setBounds(100, 30, 123, 20);
			// 初始化
			jTextFieldMinTextHeight.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						value = Integer.valueOf(jTextFieldMinTextHeight.getText());
						// 如果最小文本高度大于最大文本高度的话，抛出异常
						if (value > Double.valueOf(jTextFieldMaxTextHeight.getText())) {
							throw new Exception();
						}
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (jTextFieldMinTextHeight.getText().indexOf("0") == 0 && jTextFieldMinTextHeight.getText().length() > 1 || jTextFieldMinTextHeight
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldMinTextHeight.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMinTextHeight.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setMinTextHeight(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldMinTextHeight.getText())) {
							return;
						}
						if (!jTextFieldMinTextHeight.getText().equals(record)) {
							record = jTextFieldMinTextHeight.getText();
						}
						currentTheme.setMinTextHeight(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldMinTextHeight.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldMinTextHeight.getText().trim())
							|| (jTextFieldMinTextHeight.getText().indexOf("0") == 0 && jTextFieldMinTextHeight.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMinTextHeight.setText("0");
							}
						});
					}
				}
			});
			jPanelHeight.add(jTextFieldMinTextHeight);

			jTextFieldMaxTextHeight = new JTextField();
			jTextFieldMaxTextHeight.setBounds(100, 5, 123, 20);
			jTextFieldMaxTextHeight.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						value = Integer.valueOf(jTextFieldMaxTextHeight.getText());
						// 如果最大文本高度小于最小文本高度的话，则抛出异常
						if (value < Double.valueOf(jTextFieldMinTextHeight.getText())) {
							throw new Exception();
						}
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (jTextFieldMaxTextHeight.getText().indexOf("0") == 0 && jTextFieldMaxTextHeight.getText().length() > 1 || jTextFieldMaxTextHeight
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldMaxTextHeight.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMaxTextHeight.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setMaxTextHeight(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldMaxTextHeight.getText())) {
							return;
						}
						if (!jTextFieldMaxTextHeight.getText().equals(record)) {
							record = jTextFieldMaxTextHeight.getText();
						}
						currentTheme.setMaxTextHeight(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldMaxTextHeight.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldMaxTextHeight.getText().trim())
							|| (jTextFieldMaxTextHeight.getText().indexOf("0") == 0 && jTextFieldMaxTextHeight.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMaxTextHeight.setText("0");
							}
						});
					}
				}
			});
			jPanelHeight.add(jTextFieldMaxTextHeight);

			final JLabel jLabel = new JLabel();
			jLabel.setBounds(230, 5, 40, 12);
			jPanelHeight.add(jLabel);
			jLabel.setHorizontalAlignment(SwingConstants.LEFT);
			jLabel.setIconTextGap(0);
			jLabel.setFont(new Font("0.1mm", Font.ITALIC, 10));
			jLabel.setText("0.1mm");

			final JLabel jLabelItalic = new JLabel();
			jLabelItalic.setText("0.1mm");
			jLabelItalic.setBounds(230, 30, 40, 12);
			jLabelItalic.setFont(new Font("0.1mm", Font.ITALIC, 10));
			jPanelHeight.add(jLabelItalic);

			jTextFieldMaxTextWidth = new JTextField();
			jTextFieldMaxTextWidth.setBounds(100, 55, 123, 18);
			jPanelHeight.add(jTextFieldMaxTextWidth);
			jTextFieldMaxTextWidth.setHorizontalAlignment(JTextField.RIGHT);
			jTextFieldMaxTextWidth.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						value = Integer.valueOf(jTextFieldMaxTextWidth.getText());
						// 判断文本最大宽度是否小于最小文本宽度，如果小于则抛异常
						if (value < Double.valueOf(jTextFieldMinTextWidth.getText())) {
							throw new Exception();
						}
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (jTextFieldMaxTextWidth.getText().indexOf("0") == 0 && jTextFieldMaxTextWidth.getText().length() > 1 || jTextFieldMaxTextWidth
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldMaxTextWidth.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMaxTextWidth.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setMaxTextWidth(Integer.parseInt(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldMaxTextWidth.getText())) {
							return;
						}
						if (!jTextFieldMaxTextWidth.getText().equals(record)) {
							record = jTextFieldMaxTextWidth.getText();
						}
						currentTheme.setMaxTextWidth(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldMaxTextWidth.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldMaxTextWidth.getText().trim())
							|| (jTextFieldMaxTextWidth.getText().indexOf("0") == 0 && jTextFieldMaxTextWidth.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMaxTextWidth.setText("0");
							}
						});
					}
				}
			});

			jTextFieldMinTextWidth = new JTextField();
			jTextFieldMinTextWidth.setBounds(100, 80, 123, 18);
			jPanelHeight.add(jTextFieldMinTextWidth);
			jTextFieldMinTextWidth.setHorizontalAlignment(JTextField.RIGHT);
			jTextFieldMinTextWidth.getDocument().addDocumentListener(new DocumentListener() {
				private String record = "0";
				// 添加该变量用于优化
				private boolean isException = false;

				public void changedUpdate(DocumentEvent e) {
				}

				public void insertUpdate(DocumentEvent e) {
					int value = 0;
					try {
						value = Integer.valueOf(jTextFieldMinTextWidth.getText());
						// 判断最小文本宽度是否大于最大文本宽度，如果大于则抛异常
						if (value > Double.valueOf(jTextFieldMaxTextWidth.getText())) {
							throw new Exception();
						}
						isException = false;
						// 处理各种情况
						if (value < 0
								|| (jTextFieldMinTextWidth.getText().indexOf("0") == 0 && jTextFieldMinTextWidth.getText().length() > 1 || jTextFieldMinTextWidth
										.getText().indexOf("-") == 0)) {
							throw new Exception();
						}
						record = jTextFieldMinTextWidth.getText();
					} catch (Exception exp) {
						isException = true;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMinTextWidth.setText(record);
							}
						});
					}
					if (isException == false) {
						currentTheme.setMinTextWidth(Integer.parseInt(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}

				public void removeUpdate(DocumentEvent e) {
					if (isException == false) {
						if ("".equals(jTextFieldMinTextWidth.getText())) {
							return;
						}
						if (!jTextFieldMinTextWidth.getText().equals(record)) {
							record = jTextFieldMinTextWidth.getText();
						}
						currentTheme.setMinTextWidth(Integer.valueOf(record));
						jPanelPreView.refreshPreViewMapControl(currentTheme);
					}
				}
			});
			jTextFieldMinTextWidth.addFocusListener(new FocusAdapter() {
				public void focusLost(FocusEvent e) {
					if ("".equals(jTextFieldMinTextWidth.getText().trim())
							|| (jTextFieldMinTextWidth.getText().indexOf("0") == 0 && jTextFieldMinTextWidth.getText().length() > 1)) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								jTextFieldMinTextWidth.setText("0");
							}
						});
					}
				}
			});

			maxTextWidth.setBounds(10, 55, 81, 18);
			jPanelHeight.add(maxTextWidth);

			minTextWidth.setBounds(10, 80, 81, 18);
			jPanelHeight.add(minTextWidth);

			final JLabel jLabelFontItalic = new JLabel();
			jLabelFontItalic.setBounds(230, 55, 40, 12);
			jPanelHeight.add(jLabelFontItalic);
			jLabelFontItalic.setFont(new Font("0.1mm", Font.ITALIC, 10));
			jLabelFontItalic.setText("0.1mm");

			final JLabel jLabelTemp = new JLabel();
			jLabelTemp.setBounds(230, 80, 40, 12);
			jPanelHeight.add(jLabelTemp);
			jLabelTemp.setFont(new Font("0.1mm", Font.ITALIC, 10));
			jLabelTemp.setText("0.1mm");
		}
		return jPanelHeight;
	}

	/**
	 * 返回专题图
	 */
	protected Theme getTheme() {
		return currentTheme;
	}

	/**
	 * 重新设置专题图
	 */
	protected void setTheme(Theme theme) {
		currentTheme = (ThemeLabel) theme;

		// 初始化
		if (currentTheme.getOverLengthMode().equals(OverLengthLabelMode.NONE)) {
			jComboBoxOverLengthLabelMode.setSelectedIndex(0);
			if (jSpinnerSingleMaxLabelLength.isEnabled()) {
				jSpinnerSingleMaxLabelLength.setEnabled(false);
			}
		} else if (currentTheme.getOverLengthMode().equals(OverLengthLabelMode.NEWLINE)) {
			jComboBoxOverLengthLabelMode.setSelectedIndex(1);
		} else {
			jComboBoxOverLengthLabelMode.setSelectedIndex(2);
		}

		// 初始化
		NumberEditor numberEditor = (NumberEditor) jSpinnerSingleMaxLabelLength.getEditor();
		numberEditor.getFormat().applyPattern("0.#");
		JFormattedTextField textField = numberEditor.getTextField();
		textField.setText(String.valueOf(currentTheme.getMaxLabelLength()));

		// 初始化
		size2D = currentTheme.getTextExtentInflation();
		String temp = String.valueOf(size2D.getWidth());
		jTextFieldTextExtentInflationX.setText(temp.substring(0, temp.indexOf(".")));

		String value = String.valueOf(size2D.getHeight());
		jTextFieldTextExtentInflationY.setText(value.substring(0, value.indexOf(".")));

		// 初始化
		jTextFieldMaxTextHeight.setText(String.valueOf(currentTheme.getMaxTextHeight()));
		jTextFieldMaxTextHeight.setHorizontalAlignment(JTextField.RIGHT);

		jTextFieldMinTextHeight.setText(String.valueOf(currentTheme.getMinTextHeight()));
		jTextFieldMaxTextWidth.setText(String.valueOf(currentTheme.getMaxTextWidth()));
		jTextFieldMinTextWidth.setText(String.valueOf(currentTheme.getMinTextWidth()));

		jCheckBoxAlongLine.setSelected(currentTheme.isAlongLine());
		if (!jPanelPreView.getDataset().getType().equals(DatasetType.LINE)) {
			jCheckBoxAlongLine.setEnabled(false);
		}

		jCheckBoxAngleFixed.setEnabled(jCheckBoxAlongLine.isSelected());
		if (jCheckBoxAngleFixed.isSelected()) {
			jCheckBoxAngleFixed.setSelected(currentTheme.isAngleFixed());
		}

		jComboBoxAlongLineDirection.setEnabled(jCheckBoxAlongLine.isSelected());
		jLabelAlongLineDirection.setEnabled(jCheckBoxAlongLine.isSelected());
		if (jComboBoxAlongLineDirection.isEnabled()) {
			AlongLineDirection tempAlongLineDirection = currentTheme.getAlongLineDirection();
			if (tempAlongLineDirection.equals(AlongLineDirection.LEFT_TOP_TO_RIGHT_BOTTOM)) {
				jComboBoxAlongLineDirection.setSelectedIndex(0);
			} else if (tempAlongLineDirection.equals(AlongLineDirection.LEFT_BOTTOM_TO_RIGHT_TOP)) {
				jComboBoxAlongLineDirection.setSelectedIndex(1);
			} else if (tempAlongLineDirection.equals(AlongLineDirection.RIGHT_BOTTOM_TO_LEFT_TOP)) {
				jComboBoxAlongLineDirection.setSelectedIndex(2);
			} else if (tempAlongLineDirection.equals(AlongLineDirection.RIGHT_TOP_TO_LEFT_BOTTOM)) {
				jComboBoxAlongLineDirection.setSelectedIndex(3);
			} else if (tempAlongLineDirection.equals(AlongLineDirection.ALONG_LINE_NORMAL)) {
				jComboBoxAlongLineDirection.setSelectedIndex(4);
			}
		}

		NumberEditor numberEditor2 = (NumberEditor) jSpinnerLongLineSpaceRatio.getEditor();
		numberEditor2.getFormat().applyPattern("0.#");
		JFormattedTextField textField2 = numberEditor2.getTextField();
		if (jSpinnerLongLineSpaceRatio.isEnabled()) {
			textField2.setText(String.valueOf(currentTheme.getAlongLineSpaceRatio()));
		}

		jCheckBoxRepeatIntervalFixed.setEnabled(jCheckBoxAlongLine.isSelected());
		if (jCheckBoxRepeatIntervalFixed.isEnabled()) {
			jCheckBoxRepeatIntervalFixed.setSelected(currentTheme.isRepeatIntervalFixed());
		}

		jTextFieldRepeatInterval.setEnabled(jCheckBoxAlongLine.isSelected());
		jLabelRepeatInterval.setEnabled(jCheckBoxAlongLine.isSelected());
	}
}
