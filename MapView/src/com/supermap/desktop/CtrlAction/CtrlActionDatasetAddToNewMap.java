package com.supermap.desktop.CtrlAction;

import javax.swing.tree.DefaultMutableTreeNode;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.enums.WindowType;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.WorkspaceComponentManager;
import com.supermap.desktop.ui.controls.TreeNodeData;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Map;
import com.supermap.ui.Action;

public class CtrlActionDatasetAddToNewMap extends CtrlAction {

	public CtrlActionDatasetAddToNewMap(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		try {
			Dataset[] datasets = Application.getActiveApplication().getActiveDatasets();
			String name = CommonToolkit.MapWrap
					.getAvailableMapName(String.format("%s@%s", datasets[0].getName(), datasets[0].getDatasource().getAlias()), true);
			IFormMap formMap = (IFormMap) CommonToolkit.FormWrap.fireNewWindowEvent(WindowType.MAP, name);
			if (formMap != null) {
				Map map = formMap.getMapControl().getMap();
				for (Dataset dataset : datasets) {
					if (dataset.getType() != DatasetType.TABULAR && dataset.getType() != DatasetType.TOPOLOGY) {
						map.getLayers().add(dataset, true);
					}
				}
				map.refresh();
				UICommonToolkit.getLayersManager().setMap(map);

				// add by huchenpu 20150716
				// 新建的地图窗口，修改默认的Action为漫游
				formMap.getMapControl().setAction(Action.PAN);
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		boolean enable = false;
		try {
			Dataset[] datasets = Application.getActiveApplication().getActiveDatasets();
			if (datasets != null && datasets.length > 0 && datasets[0].getType() != DatasetType.TABULAR && datasets[0].getType() != DatasetType.TOPOLOGY) {
				enable = true;
			}
		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
		return enable;
	}

}
